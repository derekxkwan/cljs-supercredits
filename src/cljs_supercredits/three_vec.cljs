(ns cljs-supercredits.three-vec
  (:require [cljsjs.three]))

(def THREE js/THREE)

(defn threevec-to-vec [threevec]
  [(.-x threevec) (.-y threevec) (.-z threevec)])

(defn vec-to-threevec [[x y z]]
  (THREE.Vector3. x y z))

(defn rand-vec [dim lo hi]
  (let [cur-range (Math/abs (- hi lo))]
    (apply vector (repeatedly dim #(+ (rand cur-range) lo)))
  ))

(defn add-vec [a & b] (apply map + a b))

(defn sub-vec [a & b] (apply map - a b))

(defn mult-vec [a & b] (apply map * a b))

(defn sclmult-vec [vec scl] (map #(* % scl) vec))

(defn div-vec [a & b] (apply map / a b))

(defn scldiv-vec [vec scl] (map #(/ % scl) vec))

(defn mag-vec [vec] (.sqrt js/Math (reduce + (map #(* % %) vec))))

(defn dist-vec [vec1 vec2]
  "distance between two vecs"
  (.sqrt js/Math (reduce + (map #(* % %) (sub-vec vec1 vec2)))))

(defn norm-vec [vec]
  (let [magn (mag-vec vec)]
    (if (not= magn 0)
      (scldiv-vec vec magn)
      vec)))

(defn lim-vec [vec lim]
  "limit magnitude of vec"
  (if (> (mag-vec vec) lim)
    (sclmult-vec (norm-vec vec) lim)
    vec))

(defn transpose-vec [coll]
  (apply map vector coll))

