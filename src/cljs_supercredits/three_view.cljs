(ns cljs-supercredits.three-view
  (:require [cljsjs.three]))

(def THREE js/THREE)

;; .- is property accessor
(defn perspec-camera [ width height [x y z] & {:keys [angle near far] :or {angle 45, near 0.1, far 1000}}]
  (let [c (THREE.PerspectiveCamera. angle (/ width height) near far)]
    (.set (.-position c) x y z)
    ;;(set! js/CAMERA c)
    c))

;; #js { creates javascript object
(defn renderer [canvas-elt width height pix-ratio & {:keys [clear-color clear-alpha] :or {clear-color 0x000000, clear-alpha 1}}]
  (let [r (THREE.WebGLRenderer. #js {:antialias true :alpha true :canvas canvas-elt})]
    (.setClearColor r clear-color clear-alpha)
    (.setSize r width height)
    (.setPixelRatio r pix-ratio)
    ;;(.appendChild canvas-elt (.-domElement r))
    ;;(set! js/RENDERER r)
    r))

(defn mx->wx [x canvas-width visible-width]
  (- (/ x (/ canvas-width visible-width))
     (/ visible-width 2)))

(defn my->wy [y canvas-height visible-height]
  (- (/ y (canvas-height visible-height))
     (/ visible-height 2)))

(comment 
(defn render [renderer scene camera & {:keys [updatefn] :or {updatefn #()}}]
  (.requestAnimationFrame js/window render)
  (updatefn)
  (.render renderer scene camera))
)

(defn scene []
  (THREE.Scene.))

(defn camera-look-at! [camera [x y z]]
  (.lookAt camera x y z))

(defn position-set! [three-obj [x y z]]
  (.set (.-position three-obj) x y z))

(defn scene-add! [scene three-obj]
  (.add scene three-obj))

(defn rotation-set! [three-obj [x y z]]
  (.set (.-rotation three-obj) x y z))

(defn scale-set! [three-obj [x y z]]
  (.set (.-scale three-obj) x y z))

(defn position-get [three-obj]
  (let [pos (.-position three-obj)]
    [(.-x pos) (.-y pos) (.-z pos)]))

(defn rotation-get [three-obj]
  (let [rot (.-rotation three-obj)]
    [(.-x rot) (.-y rot) (.-z rot)]))

(defn scale-get [three-obj]
  (let [scl (.-scale three-obj)]
    [(.-x scl) (.-y scl) (.-z scl)]))



