(ns cljs-supercredits.core
  (:require [cljsjs.three]
            [cljs-supercredits.three-view :as view]
            [cljs-supercredits.three-vec :as vec]))

(def view-angle 70)
(def view-near 0.1)
(def view-far 1000)
(def num-stars 3600)
(def star-size 1)
(def font-path "../DejaVuSans_Bold.json")
(def cur-text (atom "BOBSON DUGNUTT"))
(def my-text)

(def THREE js/THREE)

(def c-elt (.getElementById js/document "mainc"))
(set! (.-width c-elt) (.-innerWidth js/window))
(set! (.-height c-elt) (.-innerHeight js/window))
(def WIDTH (.-width c-elt))
(def HEIGHT (.-height c-elt))

(def scene (view/scene))

(def camera (view/perspec-camera WIDTH HEIGHT [0 0 50] :angle view-angle :near view-near :far view-far))

(def renderer (view/renderer c-elt WIDTH HEIGHT (.-devicePixelRatio js/window) :clear-color 0x000000))

(def star-system
  (let [verts (repeatedly num-stars #(vec/vec-to-threevec (vec/rand-vec 3 -250 250)))
        geom (THREE.Geometry.)
        mat (THREE.ParticleBasicMaterial. #js {:color 0xffffff :size star-size})]
    (set! (.-vertices geom) (into-array verts))
    (THREE.ParticleSystem. geom mat)))

;;(view/add-point-light! scene [0 0 10])

(def my-pt-light (THREE.PointLight. 0xffffff))

(view/position-set! my-pt-light [-10 15 50])


(defn define-text [callback-font]
 (let [geom  (THREE.TextGeometry. @cur-text #js {
                            :font callback-font
                            :size 5
                            :height 5
                            :curveSegments 12
                            :bevelEnabled false
                            :bevelThickness 10
                            :bevelSize 3
                            :bevelSegments 5})
       bbox (.computeBoundingBox geom)
       offset (* -0.5 (- (-> geom .-boundingBox .-max .-x) (-> geom .-boundingBox .-min .-x)))
       mat (THREE.MeshPhongMaterial. #js {:color 0x0011ff})]
   (set! my-text (THREE.Mesh. geom mat))
   (view/position-set! my-text [offset 0 0])
   (view/scene-add! scene my-text)))



(def my-font (THREE.FontLoader.))
(.load my-font font-path define-text)

(doall (map #(view/scene-add! scene %) [star-system my-pt-light]))

(defn updatefn []
  (let [stars-rot (view/rotation-get star-system)
        stars-pos (view/position-get star-system)]
    (view/rotation-set! star-system (vec/add-vec [0.0001 0.0001 0.0001] stars-rot))
    
    ))


(defn render []
  (.requestAnimationFrame js/window render)
  (updatefn)
  (.render renderer scene camera))

(render)
